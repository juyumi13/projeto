# Projeto Final - SO
### Curso: Ciência da Computação
### 2º semestre
### Aluna: Júlia Yumi Umeda de Moura


## Objetivo:
    Apresentar uma solução para uma concorrência que ocorre em uma operação de transferência de fundos. O programa deve
    ser possível rodar em pelo menos dois dos três sistemas operacionais dados pelo professor, GNU/Linux, Unix/POSIX ou 
    Windows.

## O Código:
        #define _GNU_SOURCE
        #include <stdlib.h>
        #include <malloc.h>
        #include <sys/types.h>
        #include <sys/wait.h>
        #include <signal.h>
        #include <sched.h>
        #include <stdio.h>
        #define FIBER_STACK 1024*64
    
        struct c {
        int saldo;
        };
        typedef struct c conta;

        conta from, to;
        int valor;

        int transferencia( void *arg)
        {
        if (from.saldo >= valor){ 
            from.saldo -= valor;
            to.saldo += valor;
        }
        printf("Transferência concluída com sucesso!\n");
        printf("Saldo de c1: %d\n", from.saldo);
        printf("Saldo de c2: %d\n", to.saldo);
        
        return 0;
    }

    int main()
    {
        void* stack;
        pid_t pid;
        int i;

        stack = malloc( FIBER_STACK );
        if ( stack == 0 )
        {
            perror("malloc: could not allocate stack");
            exit(1);
        }

        from.saldo = 100;
        to.saldo = 100;
    
        printf( "Transferindo 10 para a conta c2\n" );
        valor = 10;
    
        for (i = 0; i < 10; i++) {
            pid = clone( &transferencia, (char*) stack + FIBER_STACK,
            SIGCHLD | CLONE_FS | CLONE_FILES | CLONE_SIGHAND | CLONE_VM, 0 );
        if ( pid == -1 )
        {
            perror( "clone" );
            exit(2);
        }
    }

    free( stack );
    printf("Transferências concluídas e memória liberada.\n");
    return 0;
    }
    
## 1- biblioteca <pthread.h>
    Pede-se que tenha 100 transações ocorrendo ao mesmo tempo, então para isso será usado os Threads que são tarefas 
    divididas que podem ser executadas de forma simultânea, para que isso seja possível no programa é acrescentado a
    biblioteca <pthread.h>.

## 2- const int NUMERO_DE_THREADS 100
    O número de transações deve ser 100 então defini o NUMERO_DE_THREADS 100 e foi utilizado no momento da criação
    desses threads. No const int, o const é um qualificador de tipo e o int é um especificador de tipo.

## 3- pthread_t threads[]
    O pthread é um padrão de threads para o POSIX e é um padrão que permite que identifique o thread.
    
## 4- pthread_create(&threads[i], NULL, transferencia, NULL)
    A função pthread_create é usada para criar o thread, para que isso seja possível, ele recebe o argumento de um
    endereço de um determinado pthread_t, o endereço da função que será executado, e o NULL foi colocado para dizer
    que o thread pode ser mudado ou configurado.
    
## 5- biblioteca <semaphore.h>, sem_wait(), sem_post()
    Essa biblioteca define o tipo sem_t usado na execução de operações de semáfaros. O sem_wait bloqueia o semáfaro
    apontado por sem, o sem_post faz o contrário, ele desbloqueia.
    
## 6- pthread_join, void* thread_res
    Em um programa em que mais de um thread está sendo executado ao mesmo tempo, é necessário que haja a finalização 
    de outro e isso pode ser feito com o uso dessa função pthread_join e nos parametros dela é dado o thread que deseja
    ser finalizado e o retorno é passado pelo ponteiro thread_res.

## 7- biblioteca <string.h>, strcpy
    A biblioteca contém uma série de funções para manipular strings, o strcpy é uma string que realiza a cópia do 
    conteúdo de uma variável a outra e ambas devem ser strings.
    
## 8- char*
    Em c para representar uma string deve-se criar um vetor de caracteres, resumindo, um vetor do tipo char, que nessa
    parte (char*) está apontando pra outro.
    
## 9- malloc
    É uma abreviação para *memory allocation* que aloca um espaço para um bloco de bytes consecutivos na memória RAM do
    computador e devolve o endereço desse bloco. O número de bytes é especificado dentro dos parâmetros da função.
    
## 10- pthread_exit()
    A função pthread_exit finaliza a thread de chamada e retorna um valor por meio do ret.
    
## 11- sem_init(), sem_destroy()
    sem_init é uma função que inicializa o semáfaro no endereço apontado pelo primeiro parâmetro (&mutex), depois o valor
    inicial do semáfaro. sem_destroy destrói o semáfaro antes iniciado apontado pelo parametro.
    
## 12- if{
    Um if dentro da função principal para caso o ret receba um valor diferente 0, ou seja, -1 dá a mensagem de erro na
    criação da thread e o outro if referente ao pthread_join tratando do mesmo erro caso retorne um valor -1 dando a
    mensagem de erro na saída do thread.

### Observação:
    Significado de pid(process identification, ou identificador de processos): cada processo tem um valor diferente
    de pid, é como se fosse o RG, a identificação de cada thread. No programa a seguir foi nomeado de ret.

  
## Para a execução do programa na plataforma Linux e Unix:
    1.  Criar um diretório:
            Para criar um diretório/pasta coloca-se o mkdir no terminal e o nome da pasta que você deseja criar de
            preferencia com um .c no final e após isso para ter a certeza de que o diretório foi criado digite o 
            comando ls que mostra os diretórios criados. 
    2.  Função cd:
            A função cd faz o acesso a uma determinada pasta, coloca-se o cd e depois o nome da pasta recém criada. Ou
            caso já tenha uma pasta criada antes coloque o cd e o nome desta pasta.
    3.  nano:
            A função nano é um editor de texto, ou seja, se deseja alterar ou incluir algo na pasta é utilizada essa
            função. nano e o nome da pasta. Dentro do nano é colocado o código 
    4.  ctrl+o e ctrl+x:
            Para gravar a alteração dentro da pasta ctrl + o e enter e para voltar o ctrl + x.
    5.  Compilar o programa:
             Para compilar programas em C com a biblioteca pthreads deve-se inserir o comando -lpthread ao GCC:
        ->  gcc prog.c  – lpthread -o prog
    6.  Na execução:
        ->    ./prog

## Para execução no Windows:
    1. Executar:
            Acessar o executar do Windows com o botão com o símbolo do Windows e a tecla R, digite no espaço cmd + enter.
    2. Diretório:
            Digite cd c:\ para alterar o diretório de trabalho atual para a raiz da unidade. Depois md c:\simple para criar
            diretório e então o c:\simple para mudar para esse diretório. simple é um nome que pode ter alterado de acordo 
            com o usuário.
    3. Compilar: 
            Para compilar é necessário que digite cl simple.c no prompt de comando. 
    4. Execução final:
            Digite simple.exe
    5. Informações:
            Retirado do site da Microsoft.
            [](https://docs.microsoft.com/pt-br/cpp/build/walkthrough-compile-a-c-program-on-the-command-line?view=vs-2019)
            
## Comprovação:
    O programa terá 100 de saldo e cada transferência terá um valor de 10, ou seja, só será possível efetuar a transação 
    10 vezes mas isso não quer dizer que as outras threads não possam ser criadas. Na saída do programa aparecerá o saldo
    da primeira conta para a outra 10 vezes com a identificação do thread, depois disso aparecerá mais 90 mensagens de 
    threads criadas com as suas identificações e no final mensagens "Transferências concluídas e memória liberada".
    
## A escolha da linguagem:
    Apesar de ter um conhecimento maior na linguagem python optei pelo desafio de procurar resolver o problema e também 
    novas funções da qual me deram novos conhecimentos sobre a linguagem C e como mencionado no documento no classroom
    o desenvolvimento do projeto pode ser facilitado pela linguagem C pelo que foi dado em sala de aula durante as aulas
    e para apresentar os requisitos necessários.
    
## Testes:
    Todos os testes feitos do programa para averiguar o seu funcionamento foram feitos pela máquina virtual VMware e
    pelo terminal do ubuntu (LINUX) disponível em computadores na universidade.

## Fontes usadas para a solução:
    [](https://homepages.dcc.ufmg.br/~coutinho/pthreads/ProgramandoComThreads.pdf)
    [](https://homepages.dcc.ufmg.br/~rimsa/documents/decom009/lessons/Aula09.pdf)
    [](https://fabiojrcosta.wordpress.com/2014/07/24/programacao-concorrente-em-c/)
    [](http://www.ic.unicamp.br/~norton/disciplinas/mc1022s2005/03_11.html)
    [](https://www.programacaoprogressiva.net/2014/09/A-Chamada-de-Sistema-fork-Como-Criar-e-Gerenciar-Processos.html)
    [](https://www.embarcados.com.br/threads-posix/)
    [](http://man7.org/linux/man-pages/man3/sem_post.3.html)
    Tanenbaum, A. S. and Machado Filho, N. Sistemas Operacionais Modernos. PEARSON, 2010. Galvin, P. B., Gagne, G., 
    and Silberschatz, A. Operating System Concepts. John Wiley & Sons, Inc. (2013).
    [](http://linguagemc.com.br/string-em-c-vetor-de-caracteres/)
    [](https://www.ime.usp.br/~pf/algoritmos/aulas/aloca.html)
    [](http://man7.org/linux/man-pages/man3/pthread_exit.3.html)
    [](https://codeforwin.org/2015/05/list-of-all-format-specifiers-in-c-programming.html)
    [](http://man7.org/linux/man-pages/man3/sem_destroy.3.html)
